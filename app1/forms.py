# from django import forms

def anketa_answer(income, workers, accounting, excise):
    orgform=''
    comment=''
    comment_add=''
    aboutbutton=''
    ref=''
    if income or workers or excise:
        orgform = 'индивидуальное предпринимательство'
        aboutbutton = 'индвидуального предпринимателя'
        ref='/ind_entr'

        income_ie_only  = income and not(workers or excise)
        workers_ie_only = workers and not(income or excise)
        # excise_ie_only  = excise and not(income or workers)
        income_se_only  = not income and workers and excise
        # workers_se_only = not workers and income and excise
        # excise_se_only  = not excise and income and workers

        if not accounting:
            comment += 'Однако обратите внимание на необходимость ведения учёта доходов.' #\ 
                      #'и подачи налоговых деклараций.'
            if workers_ie_only:
                comment_add += 'Если же Вы откажетесь от найма работников по трудовым договорам, '\
                               'то Вам подойдет самозанятость.'

            elif income_ie_only:
                comment_add += 'Если окажется, что Ваш ежемесячный доход будет составлять '\
                               'менее 200 тысяч рублей, то Вам подойдет самозанятость.'
            elif income_se_only:
                comment_add += 'Если Ваш ежемесячный доход превысит 200 тысяч рублей, то менять '\
                               'организационно-правовую форму не придется.'

        elif workers_ie_only:
            comment += 'Если же Вы откажетесь от найма работников по трудовым договорам, '\
                       'то Вам подойдет самозанятость.'
        elif income_se_only:
            comment += 'Если Ваш ежемесячный доход превысит 200 тысяч рублей, то менять '\
                       'организационно-правовую форму не придется.'

    else:
        orgform = 'самозанятость'
        aboutbutton = 'самозанятого'
        ref='/self_empl'

    context={"orgform": orgform, 
             "comment": comment, "comment_add": comment_add,
             "aboutbutton": aboutbutton, "ref": ref}
    return context
