from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render
from django.http import Http404
from random import randint
from .forms import anketa_answer

def index(request):
    return render(request, "index.html")

def self_empl(request):
    return render(request, "self_empl.html")

def why_official(request):
    return render(request, "why_official.html")

def ind_entr(request):
    return render(request, "ind_entr.html")

def offer(request):
    return render(request, "offer.html")

def contracts(request):
    return render(request, "contracts.html")

def platforms(request):
    return render(request, "platforms.html")

def termination(request):
    return render(request, "termination.html")

def main(request):
    return render(request, "index.html")


def personal_info(request):
    return render(request, "personal_info.html")

def marketing(request):
    return render(request, "marketing.html")

def cookie_politics(request):
    return render(request, "cookie_politics.html")

def excise(request):
    return render(request, "excise.html")

def about(request):
    return render(request, "about.html")

def anketa(request):
    if request.method == 'GET':
        hidden = request.GET.get('hid', default=None)
        if hidden != None:
            income      = True if request.GET.get('income', default='off')      == 'on' else False
            workers     = True if request.GET.get('workers', default='off')     == 'on' else False
            accounting  = True if request.GET.get('accounting', default='off')  == 'on' else False
            excise      = True if request.GET.get('excise', default='off')      == 'on' else False
            context = anketa_answer(income, workers, accounting, excise)
            if context['comment'] or context['comment_add']:
                return render(request, "answer.html", context)
            return render(request, "answer_without_comment.html", context)
        return render(request, 'anketa.html')
    elif request.method == 'POST':
        raise Http404("Bad request")

# def thanks(request, name='Таинственный гость', comment=''):
#     data = {'name': name, 'comment':comment}
#     return render(request, "thanks.html", context=data)

def todolist(request):
    if request.method == 'GET':
        steps = {'st1': 0, 'st2': 0, 'st3': 0, 'st4': 0, 'st5': 0, 
                 'st6': 0, 'st7': 0, 'st8': 0, 'st9': 0, 'st10': 0, 'st11': 0}
        for i in range(1, 12):
            param = 'st'+str(i)
            try: 
                steps[param] = request.COOKIES[param]
            except KeyError:
                pass
        response = render(request, "todolist.html", context=steps)
        return response
    elif request.method == 'POST':
        steps = {'st1': 0, 'st2': 0, 'st3': 0, 'st4': 0, 'st5': 0, 
                 'st6': 0, 'st7': 0, 'st8': 0, 'st9': 0, 'st10': 0, 'st11': 0}
        for i in range(1, 12):
            name = 'step'+str(i)
            param = 'st'+str(i)
            steps[param] = '1' if request.POST.get(name, default='off')=='on' else 0     
        # request.method = 'GET'
        response = render(request, "todolist.html", context=steps)
        for key, value in steps.items():
            response.set_cookie(key, str(value), max_age=60*60*24*365*3)
        return response
